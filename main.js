function selectTab(tabObj) {
    const activeTabs = document.querySelector('.tabs-title.active')
    activeTabs.classList.remove('active')
    
    tabObj.classList.add("active")
    
    document.querySelector('.show').classList.remove('show')
    const showName = tabObj.dataset.tab
    const showContent = document.getElementById(showName).classList.add('show')
}